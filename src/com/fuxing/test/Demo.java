package com.fuxing.test;

public class Demo {
	//构成重载的几种方式
	public void add(){
		System.out.println(1);
	}
	public void add(int a){
		System.out.println(a);
	}
	public void add(String a){
		System.out.println(a);
	}
	public void add(int a,String b){
		System.out.println(a+"*"+b);
	}
	public void add(String a,int b){
		System.out.println(a+"*"+b);
	}
	public int add(int a,int b){
		return a+b;
	}
}
